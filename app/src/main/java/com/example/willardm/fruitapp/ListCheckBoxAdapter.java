/**
 * Modified from the original ListDemoAdaptor created by Jeff Ondich on 4/4/14.
 * For CS 342, Carleton College
 * Displays list view items with checkboxes
 */

package com.example.willardm.fruitapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListCheckBoxAdapter extends ArrayAdapter<String> {
    public ListCheckBoxAdapter(Context context, int resource, ArrayList<String> listItems) {
        super(context, resource, listItems);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.checkbox_list_cell, null);
        }

        if (v != null) {
            final TextView textView = (TextView) v.findViewById(R.id.listCellTextView);
            CheckBox cb = (CheckBox)v.findViewById(R.id.checkBox);

            textView.setText(this.getItem(position));
            CheckBox b = (CheckBox)v.findViewById(R.id.checkBox);
            SharedPreferences mSettings = getActivity().getSharedPreferences("Settings", 0);
            SharedPreferences.Editor editor = mSettings.edit();

            // Checks if a fruit or vegetable is favorited and checks the corresponding checkbox
            if (mSettings.getString(textView.getText().toString(),"missing") != "missing") {
                b.setChecked(true);
            }

        // When the user clicks the checkbox, favorite or unfavorite the corresponding fruit/vegetable
        cb.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox)v.findViewById(R.id.checkBox);
                SharedPreferences mSettings = getActivity().getSharedPreferences("Settings", 0);
                SharedPreferences.Editor editor = mSettings.edit();

                // Favorite
                if (cb.isChecked()){
                    Toast.makeText(getContext(),
                            "Favorited: " + textView.getText(),
                            Toast.LENGTH_SHORT).show();
                    editor.putString(textView.getText().toString(), textView.getText().toString());
                    editor.commit();
                }
                // Unfavorite
                else {
                    Toast.makeText(getContext(),
                            "Unfavorited " + textView.getText(),
                            Toast.LENGTH_SHORT).show();
                    editor.remove(textView.getText().toString());
                    editor.commit();

                }

            }
        });

        }
        return v;
    }

    public Context getActivity() {
        return getContext();
    }
}
