package com.example.willardm.fruitapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.ListActivity;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class FavoritesActivity extends ListActivity {
/*Displays favorited fruits and vegetables */
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Favorites");

        //List of items to show
        ArrayList<String> listItems = new ArrayList<String>();

        ListDemoAdapter adapter = new ListDemoAdapter(this, R.layout.list_cell, listItems);
        ListView listView = (ListView)this.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
        
        //We used the settings file to store favorites
        SharedPreferences mSettings = getApplicationContext().getSharedPreferences("Settings", 0);

        ArrayList<String> fruits = new ArrayList<String>();
        fruits.add("Apricots");
        fruits.add("Artichoke");
        fruits.add("Broccoli");
        fruits.add("Corn");
        fruits.add("Honeydew");
        fruits.add("Mango");
        fruits.add("Oranges");
        fruits.add("Pineapple");
        fruits.add("Strawberries");
        fruits.add("Beets");
        fruits.add("Bell Peppers");
        fruits.add("Blueberries");
        fruits.add("Cantaloupe");
        fruits.add("Cherries");
        fruits.add("Cucumbers");
        fruits.add("Figs");
        fruits.add("Grapefruit");
        fruits.add("Grapes");
        fruits.add("Passion Fruit");
        fruits.add("Peaches");
        fruits.add("Plums");
        fruits.add("Raspberries");
        fruits.add("Tomatoes");
        fruits.add("Watermelons");
        fruits.add("Acorn Squash");
        fruits.add("Cauliflower");
        fruits.add("Cranberries");
        fruits.add("Kumquats");
        fruits.add("Mushrooms");
        fruits.add("Pomegranate");
        fruits.add("Pumpkin");
        fruits.add("Sweet Potatoes");
        fruits.add("Turnips");
        fruits.add("Pears");
        fruits.add("Pomegranate");
        fruits.add("Kale");
        fruits.add("Kiwifruit");
        fruits.add("Tangerines");

        //We find all fruits and vegetables saved as favorites in the
        // settings file and display them in the listview
        for (int i=0; i < fruits.size(); i++) {
            if (mSettings.getString(fruits.get(i),"missing") != "missing") {
                listItems.add(fruits.get(i));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorites, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }
        //noinspection SimplifiableIfStatement
        else if (id == R.id.action_about) {
            Intent intent = new Intent(this, About.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);

    }
}
