package com.example.willardm.fruitapp;

/**
 * Created by shankl on 4/7/15.
 */
public class FruitObj {
    String name;
    int value;

    FruitObj(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return this.name;
    }
    public int getValue() {
        return this.value;
    }
    public void changeValue() {
        if (this.value == 0) {
            this.value = 1;
        }

        else this.value = 0;
    }
}
