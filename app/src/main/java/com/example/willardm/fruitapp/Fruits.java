package com.example.willardm.fruitapp;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;


public class Fruits extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> listItems = new ArrayList<String>();

        // Figure out season
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH) + 1;

        // Spring
        if (month >= 3 && month <= 5) {
            setTitle("Spring Fruits & Vegetables");
            listItems.add("Apricots");
            listItems.add("Artichoke");
            listItems.add("Broccoli");
            listItems.add("Corn");
            listItems.add("Honeydew");
            listItems.add("Mango");
            listItems.add("Oranges");
            listItems.add("Pineapple");
            listItems.add("Strawberries");
        }

        // Summer
        else if (month >= 6 && month <= 8) {
            setTitle("Summer Fruits & Vegetables");
            listItems.add("Beets");
            listItems.add("Bell Peppers");
            listItems.add("Blueberries");
            listItems.add("Cantaloupe");
            listItems.add("Cherries");
            listItems.add("Cucumbers");
            listItems.add("Figs");
            listItems.add("Grapefruit");
            listItems.add("Grapes");
            listItems.add("Passion Fruit");
            listItems.add("Peaches");
            listItems.add("Plums");
            listItems.add("Raspberries");
            listItems.add("Tomatoes");
            listItems.add("Watermelons");
        }

        // Fall
        else if (month >= 9 && month <= 11) {
            setTitle("Fall Fruits & Vegetables");
            listItems.add("Acorn Squash");
            listItems.add("Cauliflower");
            listItems.add("Cranberries");
            listItems.add("Kumquats");
            listItems.add("Mushrooms");
            listItems.add("Pomegranate");
            listItems.add("Pumpkin");
            listItems.add("Sweet Potatoes");
            listItems.add("Turnips");
    }

    // Winter
    else {
        setTitle("Winter Fruits & Vegetables");
        listItems.add("Pears");
        listItems.add("Pomegranate");
        listItems.add("Kale");
        listItems.add("Kiwifruit");
        listItems.add("Tangerines");
    }

    // Display the seasonal fruits and vegetables with checkboxes
    ListCheckBoxAdapter adapter = new ListCheckBoxAdapter(this, R.layout.checkbox_list_cell, listItems);
    ListView listView = (ListView)this.findViewById(android.R.id.list);
    listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fruits, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            return true;
        }
        //noinspection SimplifiableIfStatement
        else if (id == R.id.action_about) {
            Intent intent = new Intent(this, About.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
